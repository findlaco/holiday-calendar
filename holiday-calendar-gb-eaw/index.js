var MAY = 4, AUGUST = 7;

var core = require('holiday-calendar-core');

/* 
England and Wales Holidays
*/

var earlyMayBankHoliday = function(year) {
	return core.firstMondayHoliday(year, MAY);
}

var springBankHoliday = function(year) {
	switch(year){
		case 2002:
			return core.fixedDateHoliday(2002, 5, 4);
		case 2012:
			return core.fixedDateHoliday(2012, 5, 4);
		default:
			return core.lastMondayHoliday(year, MAY);
	}
}

var summerBankHoliday = function(year) {
	return core.lastMondayHoliday(year, AUGUST);
}

exports.holidayCalendar = function(year){

	var holidays = [ 
		{	"name" : "New Year's Day",
			"date" : core.newYearsDay(year)
		},
		{	"name" : "Good Friday",
			"date" : core.goodFriday(year)
		},
		{	"name" : "Easter Monday",
			"date" : core.easterMonday(year)
		},
		{	"name" : "Early May Bank Holiday",
			"date" : earlyMayBankHoliday(year)
		},
		{	"name" : "Spring Bank Holiday",
			"date" : springBankHoliday(year)
		},	
		{	"name" : "Summer Bank Holiday",
			"date" : summerBankHoliday(year)
		},	
		{	"name" : "Christmas Day",
			"date" : core.christmasDay(year)
		},
		{	"name" : "Boxing Day",
			"date" : core.boxingDay(year)
		}
	];

	return holidays;

};

/* England and Wales Holidays */
exports.earlyMayBankHoliday = earlyMayBankHoliday;
exports.springBankHoliday = springBankHoliday;
exports.summerBankHoliday = summerBankHoliday;

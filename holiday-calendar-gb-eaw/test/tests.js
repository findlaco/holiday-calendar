var expect    = require("chai").expect;
var index = require("../index");

var dateCheck = function (name, datesToCheck, f){

	for(i = 0; i < datesToCheck.length; i++){

		var check = function(i, date){
			return function (){
				expect(f(date.getFullYear()).getTime()).to.equal(date.getTime());			
			}
		}

		var checkFn = check(i, datesToCheck[i]);
		it('should be ' + name + ' on ' + datesToCheck[i], checkFn);

  	}

}


describe('Early May Bank Holiday', function() {

	var earlyMayBankHolidayDates = [ 
			new Date(2010, 4, 3),
			new Date(2011, 4, 2),
			new Date(2012, 4, 7),
			new Date(2013, 4, 6),
			new Date(2014, 4, 5),
			new Date(2015, 4, 4),
			new Date(2016, 4, 2),
			new Date(2017, 4, 1),
			new Date(2018, 4, 7),
			new Date(2019, 4, 6),
			new Date(2020, 4, 4)
		];

	dateCheck('Early May Bank Holiday', earlyMayBankHolidayDates, index.earlyMayBankHoliday)
  
 });

describe('Spring Bank Holiday', function() {

	var springBankHolidayDates = [ 
			new Date(2002, 5, 4),
			new Date(2010, 4, 31),
			new Date(2011, 4, 30),
			new Date(2012, 5, 4),
			new Date(2013, 4, 27),
			new Date(2014, 4, 26),
			new Date(2015, 4, 25),
			new Date(2016, 4, 30),
			new Date(2017, 4, 29),
			new Date(2018, 4, 28),
			new Date(2019, 4, 27),
			new Date(2020, 4, 25)
		];

	dateCheck('Spring Bank Holiday', springBankHolidayDates, index.springBankHoliday)
  
 });

describe('Summer Bank Holiday', function() {

	var summerBankHolidayDates = [ 
			new Date(2010, 7, 30),
			new Date(2011, 7, 29),
			new Date(2012, 7, 27),
			new Date(2013, 7, 26),
			new Date(2014, 7, 25),
			new Date(2015, 7, 31),
			new Date(2016, 7, 29),
			new Date(2017, 7, 28),
			new Date(2018, 7, 27),
			new Date(2019, 7, 26),
			new Date(2020, 7, 31)
		];

	dateCheck('Summer Bank Holiday', summerBankHolidayDates, index.summerBankHoliday)
  
 });

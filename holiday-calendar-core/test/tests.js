var expect    = require("chai").expect;
var index = require("../index");

var dateCheck = function (name, datesToCheck, f){

	for(i = 0; i < datesToCheck.length; i++){

		var check = function(i, date){
			return function (){
				expect(f(date.getFullYear()).getTime()).to.equal(date.getTime());			
			}
		}

		var checkFn = check(i, datesToCheck[i]);
		it('should be ' + name + ' on ' + datesToCheck[i], checkFn);

  	}

}

describe('First Monday Holidays', function() {
  it('should be the first of the month when the first is a Monday', function() {    
    expect(index.firstMondayHoliday(2016, 1).getTime()).to.equal(new Date(2016, 1, 1).getTime());
  });
  it('should be the seventh of the month when the first is a Tuesday', function() {    
    expect(index.firstMondayHoliday(2016, 2).getTime()).to.equal(new Date(2016, 2, 7).getTime());
  });
  it('should be the sixth of the month when the first is a Wednesday', function() {    
    expect(index.firstMondayHoliday(2016, 5).getTime()).to.equal(new Date(2016, 5, 6).getTime());
  });
  it('should be the fifth of the month when the first is a Thursday', function() {    
    expect(index.firstMondayHoliday(2016, 8).getTime()).to.equal(new Date(2016, 8, 5).getTime());
  });
  it('should be the fourth of the month when the first is a Friday', function() {    
    expect(index.firstMondayHoliday(2016, 6).getTime()).to.equal(new Date(2016, 6, 4).getTime());
  });
  it('should be the third of the month when the first is a Saturday', function() {    
    expect(index.firstMondayHoliday(2016, 9).getTime()).to.equal(new Date(2016, 9, 3).getTime());
  });
  it('should be the second of the month when the first is a Sunday', function() {    
    expect(index.firstMondayHoliday(2016, 4).getTime()).to.equal(new Date(2016, 4, 2).getTime());
  });
});

describe('Last Monday Holidays', function() {
  it('should be the last of the month when the last is a Monday', function() {    
    expect(index.lastMondayHoliday(2016, 1).getTime()).to.equal(new Date(2016, 1, 29).getTime());
  });
  it('should be the one day before the last of the month when the last is a Tuesday', function() {    
    expect(index.lastMondayHoliday(2016, 4).getTime()).to.equal(new Date(2016, 4, (31-1)).getTime());
  });
  it('should be the two days before the last of the month when the last is a Wednesday', function() {    
    expect(index.lastMondayHoliday(2016, 7).getTime()).to.equal(new Date(2016, 7, 31-2).getTime());
  });
  it('should be the three days before the last of the month when the last is a Thursday', function() {    
    expect(index.lastMondayHoliday(2016, 5).getTime()).to.equal(new Date(2016, 5, 30-3).getTime());
  });
  it('should be the four days before the last of the month when the last is a Friday', function() {    
    expect(index.lastMondayHoliday(2016, 8).getTime()).to.equal(new Date(2016, 8, 30-4).getTime());
  });
  it('should be the five days before the last of the month when the last is a Saturday', function() {    
    expect(index.lastMondayHoliday(2016, 3).getTime()).to.equal(new Date(2016, 3, 30-5).getTime());
  });
  it('should be the six days before the last of the month when the last is a Sunday', function() {    
    expect(index.lastMondayHoliday(2016, 6).getTime()).to.equal(new Date(2016, 6, 31-6).getTime());
  });
 });

describe('Fixed Date Holidays', function() {
  it('should be the on the fixed day if the fixed day is a Monday', function() {    
    expect(index.fixedDateHoliday(2016, 3, 4).getTime()).to.equal(new Date(2016, 3, 4).getTime());
  });
  it('should be the on the fixed day if the fixed day is a Tuesday', function() {    
    expect(index.fixedDateHoliday(2016, 3, 5).getTime()).to.equal(new Date(2016, 3, 5).getTime());
  });
  it('should be the on the fixed day if the fixed day is a Wednesday', function() {    
    expect(index.fixedDateHoliday(2016, 3, 6).getTime()).to.equal(new Date(2016, 3, 6).getTime());
  });
  it('should be the on the fixed day if the fixed day is a Thursday', function() {    
    expect(index.fixedDateHoliday(2016, 3, 7).getTime()).to.equal(new Date(2016, 3, 7).getTime());
  });
  it('should be the on the fixed day if the fixed day is a Friday', function() {    
    expect(index.fixedDateHoliday(2016, 3, 8).getTime()).to.equal(new Date(2016, 3, 8).getTime());
  });
  it('should be the on the following Monday if the fixed day is a Saturday', function() {    
    expect(index.fixedDateHoliday(2016, 3, 9).getTime()).to.equal(new Date(2016, 3, 11).getTime());
  });
    it('should be the on the following Monday if the fixed day is a Sunday', function() {    
    expect(index.fixedDateHoliday(2016, 3, 10).getTime()).to.equal(new Date(2016, 3, 11).getTime());
  });
 });

describe('Fixed Date Holidays with Offset', function() {
  it('should be the on the fixed day if the fixed day is a Monday with offset of Sat=2, Sun=2', function() {    
    expect(index.fixedDateHolidayWithOffsets(2016, 3, 4, 2, 2).getTime()).to.equal(new Date(2016, 3, 4).getTime());
  });
  it('should be the on the fixed day if the fixed day is a Tuesday with offset of Sat=2, Sun=2', function() {    
    expect(index.fixedDateHolidayWithOffsets(2016, 3, 5, 2, 2).getTime()).to.equal(new Date(2016, 3, 5).getTime());
  });
  it('should be the on the fixed day if the fixed day is a Wednesday with offset of Sat=2, Sun=2', function() {    
    expect(index.fixedDateHolidayWithOffsets(2016, 3, 6, 2, 2).getTime()).to.equal(new Date(2016, 3, 6).getTime());
  });
  it('should be the on the fixed day if the fixed day is a Thursday with offset of Sat=2, Sun=2', function() {    
    expect(index.fixedDateHolidayWithOffsets(2016, 3, 7, 2, 2).getTime()).to.equal(new Date(2016, 3, 7).getTime());
  });
  it('should be the on the fixed day if the fixed day is a Friday with offset of Sat=2, Sun=2', function() {    
    expect(index.fixedDateHolidayWithOffsets(2016, 3, 8, 2, 2).getTime()).to.equal(new Date(2016, 3, 8).getTime());
  });
  it('should be the on the following Monday if the fixed day is a Saturday with offset of Sat=2, Sun=2', function() {    
    expect(index.fixedDateHolidayWithOffsets(2016, 3, 9, 2, 2).getTime()).to.equal(new Date(2016, 3, 11).getTime());
  });
    it('should be the on the following Tuesday if the fixed day is a Sunday with offset of Sat=2, Sun=2', function() {    
    expect(index.fixedDateHolidayWithOffsets(2016, 3, 10, 2, 2).getTime()).to.equal(new Date(2016, 3, 12).getTime());
  });
 });

describe('New Years Day', function() {

	var newYearsDayDates = [ 
			new Date(2010, 0, 1),
			new Date(2011, 0, 3),
			new Date(2012, 0, 2),
			new Date(2013, 0, 1),
			new Date(2014, 0, 1),
			new Date(2015, 0, 1),
			new Date(2016, 0, 1),
			new Date(2017, 0, 2),
			new Date(2018, 0, 1),
			new Date(2019, 0, 1),
			new Date(2020, 0, 1)
		];

	dateCheck('New Years Day', newYearsDayDates, index.newYearsDay)
  
 });

describe('Easter Sunday', function() {

	var easterDates = [ 
			new Date(2010, 3, 4),
			new Date(2011, 3, 24),
			new Date(2012, 3, 8),
			new Date(2013, 2, 31),
			new Date(2014, 3, 20),
			new Date(2015, 3, 5),
			new Date(2016, 2, 27),
			new Date(2017, 3, 16),
			new Date(2018, 3, 1),
			new Date(2019, 3, 21),
			new Date(2020, 3, 12)
		];

	dateCheck('Easter Sunday', easterDates, index.easterSunday)
  
 });

describe('Good Friday', function() {

	var goodFridayDates = [ 
			new Date(2010, 3, 2),
			new Date(2011, 3, 22),
			new Date(2012, 3, 6),
			new Date(2013, 2, 29),
			new Date(2014, 3, 18),
			new Date(2015, 3, 3),
			new Date(2016, 2, 25),
			new Date(2017, 3, 14),
			new Date(2018, 2, 30),
			new Date(2019, 3, 19),
			new Date(2020, 3, 10)
		];

	dateCheck('Good Friday', goodFridayDates, index.goodFriday)
  
 });


describe('Easter Monday', function() {

	var easterMondayDates = [ 
			new Date(2010, 3, 5),
			new Date(2011, 3, 25),
			new Date(2012, 3, 9),
			new Date(2013, 3, 1),
			new Date(2014, 3, 21),
			new Date(2015, 3, 6),
			new Date(2016, 2, 28),
			new Date(2017, 3, 17),
			new Date(2018, 3, 2),
			new Date(2019, 3, 22),
			new Date(2020, 3, 13)
		];

	dateCheck('Easter Monday', easterMondayDates, index.easterMonday)
  
 });

describe('Christmas Day', function() {

	var xmasDayDates = [ 
			new Date(2010, 11, 27),
			new Date(2011, 11, 27),
			new Date(2012, 11, 25),
			new Date(2013, 11, 25),
			new Date(2014, 11, 25),
			new Date(2015, 11, 25),
			new Date(2016, 11, 27),
			new Date(2017, 11, 25),
			new Date(2018, 11, 25),
			new Date(2019, 11, 25),
			new Date(2020, 11, 25)
		];

	dateCheck('Christmas Day', xmasDayDates, index.christmasDay)
  
 });

describe('Boxing Day', function() {

	var boxingDayDates = [ 
			new Date(2010, 11, 28),
			new Date(2011, 11, 26),
			new Date(2012, 11, 26),
			new Date(2013, 11, 26),
			new Date(2014, 11, 26),
			new Date(2015, 11, 28),
			new Date(2016, 11, 26),
			new Date(2017, 11, 26),
			new Date(2018, 11, 26),
			new Date(2019, 11, 26),
			new Date(2020, 11, 28)
		];

	dateCheck('Boxing Day', boxingDayDates, index.boxingDay)
  
 });
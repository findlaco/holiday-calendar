/*
Generic Holidays
*/

var SUNDAY = 0, MONDAY = 1, SATURDAY =6;
var JANUARY = 0, MAY = 4, AUGUST = 7, DECEMBER = 11;

var offsetDate = function(date, offset){
	date.setDate(date.getDate() + offset);
	return date;
}

var fixedDateHoliday = function(year, month, day){
	return fixedDateHolidayWithOffsets(year, month, day, 2, 1);
}

var festiveHoliday = function(year, month, day){
	return fixedDateHolidayWithOffsets(year, month, day, 2, 2);
}

var fixedDateHolidayWithOffsets = function(year, month, day, saturdayOffset, sundayOffset){
	var date = new Date(year, month, day);
	var weekday = date.getDay();
	var offset = 0;
	switch(weekday){
		case SATURDAY:
			offset = saturdayOffset;
			break;
		case SUNDAY:
			offset = sundayOffset;
			break;
	}
	return offsetDate(date, offset);
}

var firstMondayHoliday = function(year, month){
	var date = new Date(year, month, 1);
	var weekday = date.getDay();
	var offset = 0;
	if(weekday < MONDAY){
		offset = 1;
	}
	else if(weekday == MONDAY){
		offset = 0;
	}
	else{
		offset = 8 - weekday;
	}
	return offsetDate(date, offset);
}

var lastMondayHoliday = function(year, month){
	var date = new Date(year, month+1, 1);
	date.setDate(0);

	var weekday = date.getDay();
	var offset = 0;
	if(weekday < MONDAY){
		offset = -6;
	}
	else if(weekday == MONDAY){
		offset = 0;
	}
	else{
		offset = -(weekday - 1);
	}
	return offsetDate(date, offset);
}

/*
Common Holidays
*/

var newYearsDay = function(year) {
	return fixedDateHoliday(year, JANUARY, 1);
}

var goodFriday = function(year) {
	var goodFriday = easterSunday(year);
	var offset = -2;
	goodFriday.setDate(goodFriday.getDate() + offset);
	return goodFriday;
}

var easterSunday = function (year) {
	var Y = year;
    var C = Math.floor(Y/100);
    var N = Y - 19*Math.floor(Y/19);
    var K = Math.floor((C - 17)/25);
    var I = C - Math.floor(C/4) - Math.floor((C - K)/3) + 19*N + 15;
    I = I - 30*Math.floor((I/30));
    I = I - Math.floor(I/28)*(1 - Math.floor(I/28)*Math.floor(29/(I + 1))*Math.floor((21 - N)/11));
    var J = Y + Math.floor(Y/4) + I + 2 - C + Math.floor(C/4);
    J = J - 7*Math.floor(J/7);
    var L = I - J;
    var M = 3 + Math.floor((L + 40)/44);
    var D = L + 28 - 31*Math.floor(M/4);

    return new Date(year, M-1, D);
}

var easterMonday = function(year) {
	var easterMonday = easterSunday(year);
	var offset = 1;
	easterMonday.setDate(easterMonday.getDate() + offset);
	return easterMonday;
}

var christmasDay = function(year) {
	return festiveHoliday(year, DECEMBER, 25);
}

var boxingDay = function(year) {
	return festiveHoliday(year, DECEMBER, 26);
}

exports.firstMondayHoliday = firstMondayHoliday;
exports.lastMondayHoliday = lastMondayHoliday;
exports.fixedDateHoliday = fixedDateHoliday;
exports.fixedDateHolidayWithOffsets = fixedDateHolidayWithOffsets;
exports.newYearsDay = newYearsDay;
exports.goodFriday = goodFriday;
exports.easterSunday = easterSunday;
exports.easterMonday = easterMonday;
exports.christmasDay = christmasDay;
exports.boxingDay = boxingDay;

/* 
England and Wales Holidays
*/

var earlyMayBankHoliday = function(year) {
	return firstMondayHoliday(year, MAY);
}

var springBankHoliday = function(year) {
	switch(year){
		case 2002:
			return fixedDateHoliday(2002, 5, 4);
		case 2012:
			return fixedDateHoliday(2012, 5, 4);
		default:
			return lastMondayHoliday(year, MAY);
	}
}

var summerBankHoliday = function(year) {
	return lastMondayHoliday(year, AUGUST);
}

exports.holidayCalendar = function(year){

	var holidays = [ 
		{	"name" : "New Year's Day",
			"date" : newYearsDay(year)
		},
		{	"name" : "Good Friday",
			"date" : goodFriday(year)
		},
		{	"name" : "Easter Monday",
			"date" : easterMonday(year)
		},
		{	"name" : "Early May Bank Holiday",
			"date" : earlyMayBankHoliday(year)
		},
		{	"name" : "Spring Bank Holiday",
			"date" : springBankHoliday(year)
		},	
		{	"name" : "Summer Bank Holiday",
			"date" : summerBankHoliday(year)
		},	
		{	"name" : "Christmas Day",
			"date" : christmasDay(year)
		},
		{	"name" : "Boxing Day",
			"date" : boxingDay(year)
		}
	];

	return holidays;

};

/* England and Wales Holidays */
exports.earlyMayBankHoliday = earlyMayBankHoliday;
exports.springBankHoliday = springBankHoliday;
exports.summerBankHoliday = summerBankHoliday;
